package net.yegan.contact_list.controllers;

import net.yegan.contact_list.entities.Contact;
import net.yegan.contact_list.entities.GitRepository;
import net.yegan.contact_list.repos.ContactRepo;
import net.yegan.contact_list.repos.RepoRepo;
import net.yegan.contact_list.services.igithub;
import org.springframework.data.domain.Example;
import org.springframework.data.domain.ExampleMatcher;
import org.springframework.web.bind.annotation.*;

import java.util.List;
import java.util.stream.Stream;


@RestController
public class ContactController {

    private final ContactRepo repo;
    private final RepoRepo git_repo;
    private final igithub github_service;

    ContactController(ContactRepo repo, RepoRepo gitRepo, igithub github) {
        this.repo = repo;
        this.github_service = github;
        this.git_repo = gitRepo;
    }

    @GetMapping("/ping")
    public String ping() {
        var builder = new StringBuilder("pong</br>");
        for(Contact c : repo.findAll()) {
            for(GitRepository repo : c.getRepositories()) {
                builder.append(repo.getName()).append(" | ");
            }
            builder.append("</br>");
        }
        return builder.toString();
    }
    
    @PostMapping("/contact")
    public void SaveContact(@RequestBody Contact contact) {
        repo.save(contact);
        for(GitRepository repo : github_service.GetRepos(contact.getGithub_user())) {
            repo.setOwner(contact);
            git_repo.save(repo);
        }
    }

    @PostMapping("/contact/list")
    public Stream<Object> ContactList(@RequestBody Contact contact) {
        var matcher = ExampleMatcher.matchingAny()
                .withIgnorePaths("id")
                .withStringMatcher(ExampleMatcher.StringMatcher.CONTAINING);
        return repo.findAll(Example.of(contact, matcher)).stream()
                .map(c -> new Contact()
                        .setGithub_user(c.getGithub_user())
                        .setEmail(c.getEmail())
                        .setFirst_name(c.getFirst_name())
                        .setLast_name(c.getLast_name())
                        .setOrganization(c.getOrganization())
                        .setPhone_number(c.getPhone_number())
                );
    }
    
}
