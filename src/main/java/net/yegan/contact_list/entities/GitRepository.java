package net.yegan.contact_list.entities;

import javax.persistence.*;

@Entity
@Table(name="git_repository")
public class GitRepository {
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    private long id;

    private String html_url;

    private String git_url;

    private boolean fork;

    private String name;

    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    private Contact owner;

    public Contact getOwner() {
        return owner;
    }

    public GitRepository setOwner(Contact owner) {
        this.owner = owner;
        return this;
    }

    public long getId() {
        return id;
    }

    public GitRepository setId(long id) {
        this.id = id;
        return this;
    }

    public String getName() {
        return name;
    }

    public GitRepository setName(String name) {
        this.name = name;
        return this;
    }

    public String getHtml_url() {
        return html_url;
    }

    public GitRepository setHtml_url(String html_url) {
        this.html_url = html_url;
        return this;
    }

    public String getGit_url() {
        return git_url;
    }

    public GitRepository setGit_url(String git_url) {
        this.git_url = git_url;
        return this;
    }

    public boolean getFork() {
        return this.fork;
    }

    public GitRepository setFork(boolean fork) {
        this.fork = fork;
        return this;
    }

}
