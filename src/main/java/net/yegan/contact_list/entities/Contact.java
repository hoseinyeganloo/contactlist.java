package net.yegan.contact_list.entities;

import javax.persistence.*;
import java.util.ArrayList;
import java.util.List;

@Entity(name="contact")
@Table(name="contact")
public class Contact {

    @Id
    @GeneratedValue(strategy=GenerationType.IDENTITY)
    private int id;

    private String first_name;

    private String last_name;

    private String phone_number;

    private String email;

    private String organization;

    private String github_user;

    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY)
    private List<GitRepository> repositories = new ArrayList<>();

    public List<GitRepository> getRepositories() {
        return repositories;
    }

    public int getId() {
        return id;
    }

    public Contact setId(int id) {
        this.id = id;
        return this;
    }

    public String getEmail() {
        return email;
    }

    public Contact setEmail(String email) {
        this.email = email;
        return this;
    }

    public String getFirst_name() {
        return first_name;
    }

    public Contact setFirst_name(String first_name) {
        this.first_name = first_name;
        return this;
    }

    public String getGithub_user() {
        return github_user;
    }

    public Contact setGithub_user(String github_user) {
        this.github_user = github_user;
        return this;
    }

    public String getLast_name() {
        return last_name;
    }

    public Contact setLast_name(String last_name) {
        this.last_name = last_name;
        return this;
    }

    public String getOrganization() {
        return organization;
    }

    public Contact setOrganization(String organization) {
        this.organization = organization;
        return this;
    }

    public String getPhone_number() {
        return phone_number;
    }

    public Contact setPhone_number(String phone_number) {
        this.phone_number = phone_number;
        return this;
    }
}
