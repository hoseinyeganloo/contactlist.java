package net.yegan.contact_list.services;

import net.yegan.contact_list.entities.GitRepository;
import org.springframework.web.client.RestTemplate;

public class github implements igithub {

    private static final String base_address = "https://api.github.com";
    private RestTemplate client = new RestTemplate();

    public GitRepository[] GetRepos(String username)
    {
       return client.getForObject(
               String.format("%s/users/%s/repos", base_address, username),
               GitRepository[].class
        );
    }
}
