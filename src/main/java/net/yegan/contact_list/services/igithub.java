package net.yegan.contact_list.services;

import net.yegan.contact_list.entities.GitRepository;

public interface igithub {

    GitRepository[] GetRepos(String username);

}
