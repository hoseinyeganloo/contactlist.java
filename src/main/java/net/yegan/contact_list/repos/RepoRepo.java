package net.yegan.contact_list.repos;

import net.yegan.contact_list.entities.GitRepository;
import org.springframework.data.repository.CrudRepository;

public interface RepoRepo extends CrudRepository<GitRepository, Integer> {
}
