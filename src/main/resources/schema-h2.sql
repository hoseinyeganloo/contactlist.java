DROP TABLE IF EXISTS git_repository;
DROP TABLE IF EXISTS contact;

CREATE TABLE contact (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  first_name VARCHAR(250) NOT NULL,
  last_name VARCHAR(250) NOT NULL,
  phone_number VARCHAR(12) NOT NULL,
  email VARCHAR(250) NOT NULL unique,
  organization VARCHAR(750) DEFAULT NULL,
  github_user VARCHAR(250) NOT NULL
);

CREATE TABLE git_repository (
  id INT AUTO_INCREMENT  PRIMARY KEY,
  html_url VARCHAR(250) NOT NULL,
  git_url VARCHAR(250) NOT NULL,
  fork boolean NOT NULL,
  name VARCHAR(250) NOT NULL,
  owner int references contact(id)
);